#Deploy do auto-shutdown de vms no azure

resource "azurerm_dev_test_global_vm_shutdown_schedule" "shutdownvmsrv" {
  virtual_machine_id    = azurerm_windows_virtual_machine.vmsrv.id
  location              = var.location
  enabled               = true
  daily_recurrence_time = "0000"
  timezone              = var.timezone
  notification_settings {
    enabled = false
  }
}
