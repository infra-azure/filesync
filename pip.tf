#deploy publico ip vmserver
resource "azurerm_public_ip" "PIP" {
  name                = var.pip
  resource_group_name = var.namerg
  location            = var.location

  allocation_method = "Dynamic"
  tags              = var.tags
}
