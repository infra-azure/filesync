# Uso de Variáveis

variable "namerg" {
  description = "Nome do Resource Group"
  type        = string
  default     = "rg-poc-file-sync"

}

variable "location" {
  description = "Localizacao dos Recurosos"
  type        = string
  default     = "eastus"
}

variable "tags" {
  description = "Tags nos recursos"
  type        = map(any)
  default = {
    Ambiente = "Poc File Sync"
  }

}

variable "vnet" {
  default = "vnet-poc-filesync"
}

variable "azurerm_windows_virtual_machine" {
  default = "vmsrv"
}

variable "size" {
  default = "Standard_D2s_v3"
}

variable "pip" {
  default = "pipvmsrv-pocfilesync"
}

variable "nsg" {
  default = "nsg-vmsrv"
}

variable "nic" {
  default = "nic.vmsrv"
}

variable "timezone" {

  default = "E. South America Standard Time"
}
