resource "azurerm_storage_account" "storage" {
  name                     = "stpocfilesync"
  resource_group_name      = var.namerg
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
  tags = var.tags
}

resource "azurerm_storage_container" "containerpoc" {
  name                  = "containerpocfilesync"
  storage_account_name  = azurerm_storage_account.storage.name
  container_access_type = "private"
}

resource "azurerm_storage_share" "FSShare" {
  name                 = "fspocfilesync"
  storage_account_name = azurerm_storage_account.storage.name
  depends_on           = [azurerm_storage_account.storage]
}