# Deploy RG
resource "azurerm_resource_group" "RG" {
  name     = var.namerg
  location = var.location
  tags = var.tags
}