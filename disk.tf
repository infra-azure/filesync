resource "azurerm_managed_disk" "Dados" {
  name                 = "dados"
  location             = var.location
  resource_group_name  = var.namerg
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = "2" #Tamanho do disco em gigabytes

  tags = var.tags
}
